FROM 1science/sbt
MAINTAINER Bardia Heydarinejad <bardia@rishe.co>

# Fix sh
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

## Create editor userspace
#RUN groupadd play
#RUN useradd play -m -g play -s /bin/bash
#RUN passwd -d -u play
#RUN echo "play ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/play
#RUN chmod 0440 /etc/sudoers.d/play
#RUN mkdir /home/play/Code
#RUN chown play:play /home/play/Code

## Change user, launch bash
#USER play
#WORKDIR /home/play
#CMD ["/bin/bash"]

# Expose Code volume and play ports 9000 default 9999 debug 8888 activator ui
VOLUME "/home/play/Code"
EXPOSE 9000
EXPOSE 9999
EXPOSE 8888
WORKDIR /home/play/Code

CMD ["sbt", "run"]
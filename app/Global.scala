/**
  * Created by negar on 8/24/16.
  */
import akka.actor.Props
import controllers.LeaderBoardUpdater
import models.ServiceDatabase
import play.api.{Application, GlobalSettings}
import play.api.libs.concurrent.Akka
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import org.joda.time.{Hours,DateTime}
import scala.concurrent.duration.DurationInt

object Global extends GlobalSettings {

  override def onStart(app: Application) {
    onStartScheduler(app)
  }
  def onStartScheduler(app: Application) = {
    ServiceDatabase.namespaces.getAll().map(namespaces=>namespaces.par.map{ namespace=>
    val reminderActor = Akka.system(app).actorOf(Props(new LeaderBoardUpdater(namespace.id)))
      val delay = Hours.hoursBetween(DateTime.now(),new DateTime(namespace.start_date))
    Akka.system(app).scheduler.schedule(delay.getHours%namespace.ttl hours,namespace.ttl hours, reminderActor, "onStartScheduler")})
  }
}
package models

import java.util.UUID

import com.websudos.phantom.dsl.{UUID, _}
import play.api.libs.json.{JsPath, Json, Reads}
import play.api.libs.functional.syntax._
import scala.concurrent.Future


case class Namespace(id: UUID, name: String, application_id: UUID, ttl: Int, start_date: Long)

abstract class Namespaces extends CassandraTable[ConcreteNamespaces, Namespace] {

  object id extends UUIDColumn(this) with PrimaryKey[UUID]

  object name extends StringColumn(this)

  object application_id extends UUIDColumn(this) with PartitionKey[UUID]

  object ttl extends IntColumn(this)

  object start_date extends LongColumn(this)

  def fromRow(row: Row): Namespace = {
    Namespace(
      id = id(row),
      name = name(row),
      application_id = application_id(row),
      ttl = ttl(row),
      start_date = start_date(row)
    )
  }
}

abstract class ConcreteNamespaces extends Namespaces with RootConnector {
  implicit val namespaceWriter = Json.writes[Namespace]
  implicit val namespaceReader: Reads[Namespace] = (
    (JsPath \ "name").read[String] and
      (JsPath \ "application_id").read[String] and
      (JsPath \ "ttl").read[Int] and
      (JsPath \ "start_date").read[Long]
    ) ((name, application_id, ttl, start_date) =>
    Namespace(UUID.randomUUID(), name, UUID.fromString(application_id), ttl, start_date))


  def store(namespace: Namespace): Future[ResultSet] = {
    insert.value(_.id, namespace.id)
      .value(_.name, namespace.name)
      .value(_.application_id, namespace.application_id)
      .value(_.ttl, namespace.ttl)
      .value(_.start_date, namespace.start_date)
      .future()
  }

  def getById(id: UUID, application_id: UUID): Future[Option[Namespace]] = {
    select.where(_.application_id eqs application_id).and(_.id eqs id).one()
  }

  def getByApplicationId(application_id: UUID): Future[List[Namespace]] = {
    select.where(_.application_id eqs application_id).fetch()
  }

  def getAll(): Future[List[Namespace]] = {
    select.fetch()
  }

  def deleteById(id: UUID, application_id: UUID): Future[ResultSet] = {
    delete.where(_.application_id eqs application_id).and(_.id eqs id).future()
  }

  def deleteByApplicationId(application_id: UUID): Future[ResultSet] = {
    delete.where(_.application_id eqs application_id).future()
  }
}

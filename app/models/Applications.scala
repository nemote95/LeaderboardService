package models
import java.util.UUID
import com.websudos.phantom.dsl.{UUID, _}
import play.api.libs.json.{JsPath, Json, Reads}

import scala.concurrent.Future


case class Application(id: UUID, name: String)

abstract class Applications extends CassandraTable[ConcreteApplications, Application] {
  object id extends UUIDColumn(this) with PartitionKey[UUID]
  object name extends StringColumn(this)
  def fromRow(row: Row): Application = {
    Application(
      id = id(row),
      name = name(row)
    )
  }
}

abstract class ConcreteApplications extends Applications with RootConnector {
  implicit val applicationWriter = Json.writes[Application]
  implicit val applicationReader: Reads[Application] = (JsPath \ "name").read[String].map(Application(UUID.randomUUID,_))


  def store(application: Application): Future[ResultSet] = {
    insert.value(_.id, application.id)
      .value(_.name, application.name)
      .future()
  }

  def getById(id: UUID): Future[Option[Application]] = {
    select.where(_.id eqs id).one()
  }

  def getAll: Future[List[Application]] = {
    select.fetch()}

  def deleteById(id: UUID): Future[ResultSet] = {
    delete.where(_.id eqs id).future()
  }
}

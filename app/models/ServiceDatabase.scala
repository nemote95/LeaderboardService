package models

import com.websudos.phantom.connectors.{ContactPoint, KeySpaceDef}
import com.websudos.phantom.dsl._

object Defaults {
  val Connector = ContactPoint.local.keySpace("leaderboard") //this line should be changed for clustering aims
  //  val connector = ContactPoints(hosts).keySpace("my_keyspace")
  //hosts is the list of IP addresses !

}

class ServiceDatabase(val keyspace: KeySpaceDef) extends Database(keyspace) {

  object applications extends ConcreteApplications with keyspace.Connector
  object read_leader_boards extends ConcreteReadLeaderBoards with keyspace.Connector
  object write_leader_boards extends ConcreteWriteLeaderBoards with keyspace.Connector
  object namespaces extends ConcreteNamespaces with keyspace.Connector

  applications.create.ifNotExists().future()
  namespaces.create.ifNotExists().future()
  read_leader_boards.create.ifNotExists().future()
  write_leader_boards.create.ifNotExists().future()
}

object ServiceDatabase extends ServiceDatabase(Defaults.Connector)
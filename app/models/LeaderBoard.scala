package models

/**
  * Created by negar on 8/17/16.
  */
import java.util.UUID

import com.websudos.phantom.dsl._
import play.api.libs.json.{JsPath, Json, Reads}
import play.api.libs.functional.syntax._

import scala.concurrent.Future
import org.joda.time.DateTime


case class ReadLeaderBoard(namespace_id: UUID, score:Int,user_id:Long,last_update:DateTime)

abstract class ReadLeaderBoards extends CassandraTable[ConcreteReadLeaderBoards, ReadLeaderBoard] {
  object namespace_id extends UUIDColumn(this) with PartitionKey[UUID]
  object score extends IntColumn(this) with ClusteringOrder[Int] with Descending
  object user_id extends LongColumn(this) with Index[Long]
  object last_update extends DateTimeColumn(this)
  def fromRow(row: Row): ReadLeaderBoard = {
    ReadLeaderBoard(
      namespace_id = namespace_id(row),
      score =score(row),
      user_id = user_id(row),
      last_update =last_update(row)
    )
  }
}

abstract class ConcreteReadLeaderBoards extends ReadLeaderBoards with RootConnector {
  implicit val leaderBoardWriter = Json.writes[ReadLeaderBoard]
  implicit val leaderBoardReader = Json.reads[ReadLeaderBoard]


  def getById(namespace_id: UUID): Future[List[ReadLeaderBoard]] = {
    select.where(_.namespace_id eqs namespace_id).fetch()
  }

  def getUser(namespace_id:UUID,user_id:Long): Future[Option[ReadLeaderBoard]]=
    {
      select.where(_.namespace_id eqs namespace_id).and(_.user_id eqs user_id).allowFiltering().one()}

  def getUserRank(namespace_id:UUID,score:Int): Future[Option[Long]] ={
      select.count().where(_.namespace_id eqs namespace_id).and(_.score gt score).one()
    }

  //for copy writes
  def deleteById(namespace_id:UUID): Future[ResultSet]={
    delete.where(_.namespace_id eqs namespace_id).future()
  }
  def store(leaderBoard: ReadLeaderBoard): Future[ResultSet] = {
    insert.value(_.namespace_id, leaderBoard.namespace_id)
      .value(_.score, leaderBoard.score)
      .value(_.user_id, leaderBoard.user_id)
      .value(_.last_update,leaderBoard.last_update)
      .future()
  }
}

case class WriteLeaderBoard(namespace_id: UUID, score:Int ,user_id:Long)

abstract class WriteLeaderBoards extends CassandraTable[ConcreteWriteLeaderBoards, WriteLeaderBoard] {
  object namespace_id extends UUIDColumn(this) with PartitionKey[UUID]
  object score extends IntColumn(this)
  object user_id extends LongColumn(this) with ClusteringOrder[Long]

  def fromRow(row: Row): WriteLeaderBoard = {
    WriteLeaderBoard(
      namespace_id = namespace_id(row),
      score =score(row),
      user_id = user_id(row)
    )
  }
}

abstract class ConcreteWriteLeaderBoards extends WriteLeaderBoards with RootConnector {
  implicit val leaderBoardWriter = Json.writes[WriteLeaderBoard]
  implicit val leaderBoardReader : Reads[WriteLeaderBoard] =(
    (JsPath \ "user_id").read[Long] and
      (JsPath \ "score").read[Int]
    )((user_id,score)=>WriteLeaderBoard(new UUID(0L, 0L),score,user_id))


  def store(leaderBoard: WriteLeaderBoard): Future[ResultSet] = {
    insert.value(_.namespace_id, leaderBoard.namespace_id)
      .value(_.score, leaderBoard.score)
      .value(_.user_id, leaderBoard.user_id)
      .future()
  }

  def updateScore(namespace_id:UUID,user_id:Long,score:Int):Future[ResultSet]={
    select(_.score).where(_.namespace_id eqs namespace_id).and(_.user_id eqs user_id).one().flatMap{
      case Some(prev_score) =>
        update.where(_.namespace_id eqs namespace_id).and(_.user_id eqs user_id)
                          .modify(_.score setTo(prev_score+score)).future()
      //case None
    }
  }

  //for copy writes
  def getById(namespace_id: UUID): Future[List[WriteLeaderBoard]] = {
    select.where(_.namespace_id eqs namespace_id).fetch()
  }

}

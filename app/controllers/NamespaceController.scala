package controllers

import java.util.UUID

import models.{Namespace, ServiceDatabase}
import play.api.mvc._
import play.api.libs.json._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt
import play.api.libs.concurrent.Akka
import akka.actor.Props
import org.joda.time.{DateTime, Hours}
import play.api.Play.current

object NamespaceController extends Controller {

  import ServiceDatabase.namespaces.namespaceReader
  import ServiceDatabase.namespaces.namespaceWriter

  /*
  @apiVersion 1.0.0
  @apiGroup Namespaces
  @apiName ListNamespaces
  @apiDescription get namespaces of an application
  @api {get} apps/:application_id/namespaces get namespaces

  @apiParam {String} application_id application id

  @apiSuccess {[object]} namespaces list
  @apiSuccess {String} name namespace name
  @apiSuccess {String} id namespace id
  @apiSuccess {string} application_id application id
  @apiSuccess {Integer} ttl namespace update period
  @apiSuccess {Long} start_date date to start update Scheduler (milliseconds)

  @apiUse BadRequest
  */
  def list(application_id: String) = Action.async {
    try {
      val app_id = UUID.fromString(application_id)
      ServiceDatabase.namespaces.getByApplicationId(app_id).map { namespaces => Ok(Json.toJson(namespaces)) }
    }
    catch {
      case errors: Throwable => Future.successful(
        BadRequest(Json.obj("message" -> Json.toJson(errors.getMessage()))))
    }
  }

  /*
  @apiVersion 1.0.0
  @apiGroup Namespaces
  @apiName CreateNamespace
  @apiDescription create namespace in an application
  @api {post} apps/:application_id/namespaces create namespaces

  @apiParam {String} application_id application id

  @apiSuccess {String} name namespace name
  @apiSuccess {Integer} ttl namespace update period
  @apiSuccess {Long} start_date date to start update Scheduler (milliseconds)
  
  @apiUse BadRequest
  @piUSe NotFound
  */
  def create(application_id: String) = Action.async(BodyParsers.parse.json) { request =>
    try {
      val app_id = UUID.fromString(application_id)
      ServiceDatabase.applications.getById(app_id).map {
        case Some(application) =>
          (request.body.as[JsObject] + ("application_id" -> Json.toJson(application_id))).validate[Namespace].fold(
            errors => {
              BadRequest(Json.obj("message" -> JsError.toFlatJson(errors)))
            },

            namespace => {
              ServiceDatabase.namespaces.store(namespace)
              val updateActor = Akka.system.actorOf(Props(new LeaderBoardUpdater(namespace.id)))
              val delay = Hours.hoursBetween(DateTime.now(), new DateTime(namespace.start_date))
              Akka.system.scheduler.schedule(delay.getHours hours, namespace.ttl hours, updateActor, "updateActor")

              Created(Json.obj("namespace" -> Json.toJson(namespace)))
            }
          )
        case None => NotFound
      }
    } catch {
      case errors: Throwable => Future.successful(
        BadRequest(Json.obj("message" -> Json.toJson(errors.getMessage()))))
    }
  }

  /*
  @apiVersion 1.0.0
  @apiGroup Namespaces
  @apiName DeleteNamespace
  @apiDescription delete namespace of an application
  @api {delete} apps/:application_id/namespaces/:id delete namespaces

  @apiParam {String} application_id application id
  @apiParam {String} namespace_id namespace id

  @apiUse BadRequest
  @piUSe NotFound
  */

  def delete(id: String, application_id: String) = Action.async { request =>
    try {
      val namespace_id = UUID.fromString(id)
      val app_id = UUID.fromString(application_id)
      ServiceDatabase.namespaces.deleteById(namespace_id, app_id).map { namespace => Ok }
    }
    catch {
      case errors: Throwable => Future.successful(
        BadRequest(Json.obj("message" -> Json.toJson(errors.getMessage()))))
    }
  }

  /*
  @apiVersion 1.0.0
  @apiGroup Namespaces
  @apiName GetNamespaceInformation
  @apiDescription delete namespace of an application
  @api {get} apps/:application_id/namespaces/:id delete namespaces

  @apiParam {String} application_id application id
  @apiParam {String} namespace_id namespace id

  @apiSuccess {String} name namespace name
  @apiSuccess {String} id namespace id
  @apiSuccess {String} application_id application id
  @apiSuccess {Integer} ttl namespace update period
  @apiSuccess {Long} start_date date to start update Scheduler (milliseconds)

  @apiUse BadRequest
  @piUSe NotFound
  */

  def info(id: String, application_id: String) = Action.async { request =>
    try {
      val namespace_id = UUID.fromString(id)
      val app_id = UUID.fromString(application_id)
      ServiceDatabase.namespaces.getById(namespace_id, app_id).map {
        case Some(namespace) => Ok(Json.toJson(namespace))
        case None => NotFound
      }
    }
    catch {
      case errors: Throwable => Future.successful(
        BadRequest(Json.obj("message" -> Json.toJson(errors.getMessage()))))
    }
  }

}
/**
  * Created by negar on 8/17/16.
  */
package controllers

import java.util.UUID

import models.{ReadLeaderBoard, ServiceDatabase, WriteLeaderBoard}
import org.joda.time.DateTime
import play.api.mvc._
import play.api.libs.json._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

object LeaderBoardController extends Controller {

  import ServiceDatabase.write_leader_boards.leaderBoardReader
  import ServiceDatabase.read_leader_boards.leaderBoardWriter

  def list(namespace_id: String) = Action.async {
    /*
    @apiVersion 1.0.0
    @apiGroup LeaderBoard
    @apiName LeaderBoardList
    @apiDescription Get list of applications
    @api {get} /namespaces/:namespace_id/leaderboard get list of applications

    @apiParam {String} Sorted list of users
    @apiSuccess {Object[]} List of users.
    @apiSuccess {Integer} score users' score
    @apiSuccess {Long} user_id users' id
    apiSuccess {Long} last update

    @apiUse BadRequest

    */
    try {
      ServiceDatabase.read_leader_boards.getById(UUID.fromString(namespace_id)).map { leaderBoards =>
        Ok(Json.toJson(leaderBoards))
      }
    }
    catch {
      case errors: Throwable => Future.successful(
        BadRequest(Json.obj("status" -> BAD_REQUEST, "message" -> Json.toJson(errors.getMessage()))))
    }
  }

  /*
    @apiVersion 1.0.0
    @apiGroup LeaderBoard
    @apiName LeaderBoardCreate
    @apiDescription insert users information to leader-board
    @api {post} namespaces/:namespace_id/users get list of applications

    @apiParam {String} namespace_id namespace id
    @apiParam {Object[]} List of users.
    @apiParam {Long} user_id user's id
    @apiParam {Integer} score user's score

    @apiUse BadRequest
    */

  def create(namespace_id: String) = Action.async(BodyParsers.parse.json) { request =>
    try {
      val ns_id = UUID.fromString(namespace_id)
      request.body.as[JsArray]
        .validate[List[WriteLeaderBoard]].fold(
        errors => {
          Future.successful(BadRequest(Json.obj("message" -> JsError.toFlatJson(errors))))
        },
        users => {
          users.par.map { user => ServiceDatabase.write_leader_boards.store(WriteLeaderBoard(ns_id, user.score, user.user_id)) }
          Future.successful(Created)
        }
      )

    } catch {
      case errors: Throwable => Future.successful(
        BadRequest(Json.obj("message" -> Json.toJson(errors.getMessage()))))
    }
  }

  /*
    @apiVersion 1.0.0
    @apiGroup LeaderBoard
    @apiName LeaderBoardGetUserInformation
    @apiDescription get rank and score of a user
    @api {get} namespaces/:namespace_id/user/:user_id get user's rank and score

    @apiParam {String} namespace_id namespace id
    @apiParam {Long} user_id user's id
    @apiSuccess {Long} user_id user's id
    @apiSuccess {Integer} score user's score
    @apiSuccess {Long} rank user's rank

     @apiUse BadRequest
      @apiUse NotFound
    */

  def getUser(namespace_id: String, user_id: String) = Action.async {
    try {
      val ns_id = UUID.fromString(namespace_id)
      ServiceDatabase.read_leader_boards.getUser(ns_id, user_id.toLong)
        .flatMap { case Some(user) =>
          ServiceDatabase.read_leader_boards.getUserRank(ns_id, user.score).map {
            case Some(rank) => Ok(Json.toJson(user).as[JsObject] + ("rank" -> Json.toJson(rank + 1)))
            case None => NotFound
          }
        case None => Future.successful(NotFound)
        }
    }
    catch {
      case errors: Throwable => Future.successful(
        BadRequest(Json.obj("status" -> BAD_REQUEST, "message" -> Json.toJson(errors.getMessage()))))
    }
  }

  /*
  @apiVersion 1.0.0
  @apiGroup LeaderBoard
  @apiName LeaderBoardChangeScore
  @apiDescription change a user's score
  @api {put} namespaces/:namespace_id/user/:user_id/score/:score change user's score

  @apiParam {String} namespace_id namespace id
  @apiParam {Long} user_id user's id
  @apiParam {Integer} score user's score

  @apiUse BadRequest
  */
  def changeScore(namespace_id: String, user_id: String, score: String) = Action.async {
    try {
      ServiceDatabase.write_leader_boards.updateScore(UUID.fromString(namespace_id), user_id.toLong, score.toInt)
        .map { updated_user => Ok }

    } catch {
      case errors: Throwable => Future.successful(
        BadRequest(Json.obj("status" -> BAD_REQUEST, "message" -> Json.toJson(errors.getMessage()))))
    }
  }

  /*
  @apiVersion 1.0.0
  @apiGroup LeaderBoard
  @apiName LeaderBoardChangeScore
  @apiDescription change a user's score
  @api {put} namespaces/:namespace_id/users change users' score

  @apiParam {String} namespace_id namespace id
  @apiParam {object[]} new users' information
  @apiParam {Long} user_id user's id
  @apiParam {Integer} score user's score

  @apiUse BadRequest
  */

  def changeUsersScore(namespace_id: String) = Action.async(BodyParsers.parse.json) { request =>
    try {
      val ns_id = UUID.fromString(namespace_id)
      request.body.as[JsArray]
        .validate[List[WriteLeaderBoard]].fold(
        errors => {
          Future.successful(BadRequest(Json.obj("message" -> JsError.toFlatJson(errors))))
        },
        users => {
          users.map { user => ServiceDatabase.write_leader_boards.updateScore(ns_id, user.user_id, user.score)}
        Future.successful(Ok)}
      )

    } catch {
      case errors: Throwable => Future.successful(
        BadRequest(Json.obj("message" -> Json.toJson(errors.getMessage()))))
    }
  }

  /*
  @apiVersion 1.0.0
  @apiGroup LeaderBoard
  @apiName UpdateLeaderBoard
  @apiDescription update leader-board information
  @api {put} namespaces/:namespace_id/leaderboard update leaderboard

  @apiParam {String} namespace_id namespace id

  @apiUse BadRequest
  */
  def updateLeaderBoard(namespace_id: String) = Action.async {
    try {
      val dt = DateTime.now()
      val ns_id = UUID.fromString(namespace_id)
      ServiceDatabase.read_leader_boards.deleteById(ns_id).flatMap { deleted =>
        ServiceDatabase.write_leader_boards.getById(ns_id).map {
          users => users.map(
            user => ServiceDatabase.read_leader_boards.store(ReadLeaderBoard(user.namespace_id, user.score, user.user_id, dt))
          )
        }
      }
      Future.successful(Ok)

    }
    catch {
      case errors: Throwable => Future.successful(
        BadRequest(Json.obj("status" -> BAD_REQUEST, "message" -> Json.toJson(errors.getMessage()))))
    }
  }
}
package controllers

import java.util.UUID

import models.{Application, ServiceDatabase}
import play.api.mvc.{Action, _}
import play.api.libs.json._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

object ApplicationController extends Controller {

  import ServiceDatabase.applications.applicationReader
  import ServiceDatabase.applications.applicationWriter

  def index = Action { request =>
    Ok("Hi every one")
  }

//  def list = Action {
//    /*
//    @apiVersion 1.0.0
//    @apiGroup Application
//    @apiName ApplicationList
//    @apiDescription Get list of applications
//    @api {get} /apps get list of applications
//
//    @apiSuccess {Object[]} List of applications.
//    @apiSuccess {String} id application id
//    @apiSuccess {String} name application name
//
//    */
//    request =>
//        ServiceDatabase.applications.getAll().map { applications =>
//          Ok(Json.toJson(applications))
//        }
//  }

  def create = Action.async(BodyParsers.parse.json) {
    /*
    @apiVersion 1.0.0
    @apiGroup Application
    @apiName CreateApplication
    @apiDescription Create new application
    @api {POST} /apps
    @apiHeader {String} Content-Type =application/json JSON (application/json)

    @apiParam {String} name application name

    @apiSuccess {String} name new application name
    @apiSuccess {String} id new application id

    @apiUse BadRequest
    */
    request => val applicationReq = request.body.validate[Application]
      applicationReq.fold(
        errors => {
          Future.successful(BadRequest(Json.obj("message" -> JsError.toFlatJson(errors))))
        },
        application => {
          ServiceDatabase.applications.store(application)
          Future.successful(Created(Json.obj("new_application" -> Json.toJson(application))))
        }
      )
  }

  def info(id: String) = Action.async {
    /*
    @apiVersion 1.0.0
    @apiGroup Application
    @apiName ApplicationInformation
    @apiDescription Get details of an application
    @api {get} /apps/:id get details of application

    @apiParam {String} id application id

    @apiSuccess {String} id application id
    @apiSuccess {String} name application name

    @apiUse BadRequest
    */
    try {
      val application_id = UUID.fromString(id)
      val application = ServiceDatabase.applications.getById(application_id)
      application.map {
        case Some(application) => Ok(Json.obj("application" -> Json.toJson(application)))
        case None => NotFound
      }
    }
    catch {
      case errors: Throwable => Future.successful(
        BadRequest(Json.obj("message" -> Json.toJson(errors.getMessage())))
      )
    }
  }

  def delete(id: String) = Action.async {
    /*
    @apiVersion 1.0.0
    @apiGroup Application
    @apiName ApplicationInformation
    @apiDescription delete an application
    @api {delete} /apps/:id delete an application

    @apiParam {String} id application id

    @apiUse BadRequest
    */
    try {
      val application_id = UUID.fromString(id)
      val application = ServiceDatabase.applications.deleteById(application_id)
      application.flatMap {
        application =>
          ServiceDatabase.namespaces.deleteByApplicationId(application_id).map(deleted => Ok)
      }
    }
    catch {
      case errors: Throwable => Future.successful(
        BadRequest(Json.obj("message" -> Json.toJson(errors.getMessage))))
    }
  }
}
package controllers

import com.websudos.phantom.dsl.UUID
import models.{ReadLeaderBoard, ServiceDatabase}
import org.joda.time.DateTime
import scala.concurrent.ExecutionContext.Implicits.global
import akka.actor.Actor

class LeaderBoardUpdater(namespace_id: UUID) extends Actor {

  def receive = {
    case _ => {
      val dt = DateTime.now()
      ServiceDatabase.read_leader_boards.deleteById(namespace_id)
      ServiceDatabase.write_leader_boards.getById(namespace_id).map(
        users => users.par.map(user =>
          ServiceDatabase.read_leader_boards.store(ReadLeaderBoard(user.namespace_id, user.score, user.user_id, dt))))
    }
  }
}
